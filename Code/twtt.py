import sys
import re
import HTMLParser
import NLPlib
import csv
# Aim to cleat up the 'noise'

# TODO: deal with reading from csv later, use sample data to test for now
# TODO: remember to connect it with school's database
# TODO: maybe put all tokenize together finally
# TODO: Check more about clitics from tutorial slides


# abbrev = open('/u/cs401/Wordlists/abbrev.english').read()
abbrev = open('abbrev.english').read()
# regex: /<[^>]+>/ remove html tag

# startSet=['Capt.', 'DR.', 'mrs','sr','Jr.','Prof.','vs', 'mr.', 'miss.', 'e.g.', 'i.e.'] #startset probably would not end the sentence

# 1. remove html tags and attributes
def twtt1(str):
    msg = re.compile('<[^>]+>')
    return msg.sub('', str)


# 2. replace character codes with ascii code
def twtt2(str):
    tokenize = str.split(' ')
    result = ''
    for token in tokenize:
        if '&#' in token:  # replace char codes to ascii
            token = HTMLParser.HTMLParser().unescape(token)
        result += token + ' '
    return result


# 3. remove all urls
def twtt3(str):
    tokenize = str.split(' ')
    result = ''
    for token in tokenize:
        if not re.search("(ftp:\/\/|www\.|https?:\/\/){1}[a-zA-Z0-9u00a1-\uffff0-]{2,}\.[a-zA-Z0-9u00a1-\uffff0-]{2,}"
                         "(\S*)", token):  # not url
            result += token + ' '
    return result

# 4. remove all twitter user name @, and hashtag
def twtt4(str):
    tokenize = str.split(' ')
    result = ''
    for token in tokenize:
        if token.startswith('@'):  # delete username's @
            token = token[1:]
        while '#' in token:  # delete hash at beginning
            token = token.replace('#', '')
        result += token + ' '
    return result

# 5. each sentence is on its own line
# 6. multiple punctuation does not split
def twtt5(str):
    newLine = '\n'
    tokenize = str.split(' ')
    result = ''
    for token,next in zip(tokenize, tokenize[1:]):
        # these are possible sentence boundaries
        token = re.sub("[\"]","",token)
        if len(next) > 0:
            nextFirst = next[0]
        else:
            nextFirst = ''
        if '?' in token or '!' in token or ':' in token or ';' in token:
            if token.endswith('"') or token.endswith('\''):
                token += newLine
            elif nextFirst.isupper() or nextFirst == '\'' or nextFirst == '\"':
                token += newLine

        # complicate situation happens on period
        if '.' in token:
            temp = re.sub("['\"]", "", token); # filter out punctuations in the token
            if not (temp in abbrev):
                if nextFirst.isupper() or nextFirst == '\'' or nextFirst == '\"':
                    token += newLine
        result += token + ' '
    return result

# 7. each token including punctuation and clitics, is separated by spaces
def twtt7(str):
    tokenize = str.split(' ')
    result = ''
    for token in tokenize:
        if re.sub("['\"]","",token) not in abbrev:
            if "n't" in token:
                if token == "can't":
                    result += "can n't "
                elif token == "won't":
                    result += "would n't "
                else:
                    result += token[:-3] + " n't "
            else:
                for index in range(len(token)):
                    if token[index] in '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~':
                        if token[index] != token[index-1]:
                            result += ' ' + token[index]
                        else:
                            result += token[index]
                    else:
                        result += token[index]
                result += ' '
        else:
            result += token + ' '
    return result.rstrip()

# # 8. Each token is tagged with its part-of-speech
def twtt8(str, tagger):
    tokenize = str.split(' ')
    result = ''
    for token in tokenize:
        tag = tagger.tag([token])
        result += token + '/' + tag[0] + ' '

    return result

def twtt9(str):
    return '<A='+ str +'>'


def preprocessing(filename, stuid, output):

    stuid = int(stuid) % 80
    file = open(output,'a')
    tagger = NLPlib.NLPlib()
    with open(filename) as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV:
            result = ''
            if ((stuid * 10000) <= readCSV.line_num and readCSV.line_num <= (stuid*10000+9999)) \
                    or ((800000+stuid*10000) <= readCSV.line_num and readCSV.line_num <= (800000+stuid*10000+9999)):
                print readCSV.line_num
                demarcation = row[0]
                content = row[5]
                content = twtt1(content)
                content = twtt2(content)
                content = twtt3(content)
                content = twtt4(content)
                content = twtt5(content)
                content = twtt7(content)
                content = twtt8(content, tagger)
                demarcation = twtt9(demarcation)
                result += demarcation + '\n' + content + '\n'
                file.write(result)
    file.close()

def main(argv):
    if len(sys.argv) != 4:
        print "Error: wrong number of arguments"
    else:
        input = sys.argv[1]
        stuid = sys.argv[2]
        output = sys.argv[3]
        preprocessing(input,stuid, output)

    # path = '../tweets/training.1600000.processed.noemoticon.csv'
    # path = '/u/cs401/A1/tweets/training.1600000.processed.noemoticon.csv'
    # preprocessing(path, '1001172143', 'test.twt')

if __name__ == "__main__": main(sys.argv)
